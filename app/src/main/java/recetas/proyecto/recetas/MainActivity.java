package recetas.proyecto.recetas;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.util.Pair;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.ashokvarma.bottomnavigation.BottomNavigationBar;
import com.ashokvarma.bottomnavigation.BottomNavigationItem;
import com.jetradar.multibackstack.BackStackActivity;

import java.util.ArrayList;

import recetas.proyecto.recetas.controller.compra.CompraFragment;
import recetas.proyecto.recetas.model.compra.CompraDataController;
import recetas.proyecto.recetas.model.despensa.DespensaDataController;
import recetas.proyecto.recetas.controller.despensa.DespensaFragment;
import recetas.proyecto.recetas.controller.perfil.PerfilFragment;
import recetas.proyecto.recetas.model.despensa.Ingrediente;
import recetas.proyecto.recetas.model.recetas.RecetasDataController;
import recetas.proyecto.recetas.controller.recetas.RecetasFragment;

import static com.ashokvarma.bottomnavigation.BottomNavigationBar.OnTabSelectedListener;
//actividad principal en la que se van a mostrar y reemplzar los fragmens. Hereda de BackStackActivity para gestionar mejor los fragments (libreria)
public class MainActivity extends BackStackActivity implements OnTabSelectedListener{

    //constantes de los tabs
    private static final String STATE_CURRENT_TAB_ID = "current_tab_id";
    private static final int MAIN_TAB_ID = 0;

    //bottom navigationbar, fragment actual y tabid
    private BottomNavigationBar bottomNavBar;
    private Fragment curFragment;
    private int curTabId;

    //Controladores de datos
    public DespensaDataController despensaData;
    public RecetasDataController recetasData;
    public CompraDataController compraData;

    //lista de ingredientes seleccionados para añadir
    public ArrayList<Ingrediente> ingredientesSeleccionados = new ArrayList<>();

    @Override
    protected void onCreate(Bundle state) {
        super.onCreate(state);
        setContentView(R.layout.activity_main);

        //inicializamos los controladores
        despensaData = new DespensaDataController();
        recetasData = new RecetasDataController();
        compraData = new CompraDataController();

        //configuramos la toolbar, la navigation bar y la bottom navigationbar
        setUpToolBar();
        setUpNavigationBar();
        setUpBottomNavBar();

        //si el estado es null seleccionamos el tab por defecto y mostramos su fragment correspondiente
        if (state == null) {
            bottomNavBar.selectTab(MAIN_TAB_ID, false);
            showFragment(rootTabFragment(MAIN_TAB_ID));
        }


    }
    //mostramos un color u otro en la barra de navegación dependiendo de la version de android
    private void setUpNavigationBar() {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            getWindow().setNavigationBarColor(getResources().getColor(R.color.blanco));
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_NAVIGATION_BAR | View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            return;
        } else if(android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
            getWindow().setNavigationBarColor(getResources().getColor(R.color.colorAccent));
        }
    }

    //conseguimos y configuramos la toolbar
    private void setUpToolBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

    }

    //configuramos e inicializamos la bottonnavigationbar (tabs) con su modo, estilo e iconos y ponemos esta clase como listener
    private void setUpBottomNavBar() {
        bottomNavBar = (BottomNavigationBar) findViewById(R.id.bottom_navigation);
        bottomNavBar.setMode(BottomNavigationBar.MODE_FIXED);
        bottomNavBar.setBackgroundStyle(BottomNavigationBar.BACKGROUND_STYLE_STATIC);
        bottomNavBar.setMinimumHeight(10);
        bottomNavBar.setMinimumWidth(10);
        bottomNavBar
                .addItem(new BottomNavigationItem(R.drawable.apple_fruit, R.string.title_pantry))
                .addItem(new BottomNavigationItem(R.drawable.food, R.string.title_recipes))
                .addItem(new BottomNavigationItem(R.drawable.shopping_cart, R.string.title_shopping))
                .addItem(new BottomNavigationItem(R.drawable.chef, R.string.title_profile))
                .initialise();
        bottomNavBar.setTabSelectedListener(this);
    }

    //definimos los fragments principales de cada tab por su id
    @NonNull
    private Fragment rootTabFragment(int tabId) {
        switch (tabId) {
            case 0:
                return DespensaFragment.newInstance(getString(R.string.title_pantry));
            case 1:
                return RecetasFragment.newInstance(getString(R.string.title_recipes));
            case 2:
                return CompraFragment.newInstance(getString(R.string.title_shopping));
            case 3:
                return PerfilFragment.newInstance(getString(R.string.title_profile));
            default:
                throw new IllegalArgumentException();
        }
    }

    //configuramos el conportamiento cuando la aplicación se restaure (mostrar el tab donde nos habiamos quedado)
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        curFragment = getSupportFragmentManager().findFragmentById(R.id.content);
        curTabId = savedInstanceState.getInt(STATE_CURRENT_TAB_ID);
        bottomNavBar.selectTab(curTabId, false);
    }

    //guardamos el estado de la app para cuando tengamos que restaurarla. Guardamos el id del tab actual
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putInt(STATE_CURRENT_TAB_ID, curTabId);
        super.onSaveInstanceState(outState);
    }

    //cuando se pulse Atrás, quitamos el fragment de pantalla y lo eliminamos del stack de fragments
    @Override
    public void onBackPressed() {
        Pair<Integer, Fragment> pair = popFragmentFromBackStack();
        if (pair != null) {
            backTo(pair.first, pair.second); //ejecutamos el metodo para reemplazar el fragment
        } else {
            super.onBackPressed();
        }
    }

    //este metodo reemplaza el fragment por el correspondiente tabId
    @Override
    public void onTabSelected(int position) {
        if (curFragment != null) {
            pushFragmentToBackStack(curTabId, curFragment);
        }
        curTabId = position;
        Fragment fragment = popFragmentFromBackStack(curTabId);
        if (fragment == null) {
            fragment = rootTabFragment(curTabId);
        }
        replaceFragment(fragment);
    }

    //si estamos en un tab y lo volvemos a pulsar, motramos el fragment principal
    @Override
    public void onTabReselected(int position) {
        backToRoot();
    }

    @Override
    public void onTabUnselected(int position) {}

    //metodo que nos muestra el fragment que le pasemos
    public void showFragment(@NonNull Fragment fragment) {
        showFragment(fragment, true);
    }

    //metodo que muestra el fragemnt que le pasamos y definimos con un booleano si queremos añadirlo al stack
    public void showFragment(@NonNull Fragment fragment, boolean addToBackStack) {
        if (curFragment != null && addToBackStack) {
            pushFragmentToBackStack(curTabId, curFragment);
        }
        replaceFragment(fragment);
    }

    //volvemos al fragment indicado en en tabId indicado
    private void backTo(int tabId, @NonNull Fragment fragment) {
        if (tabId != curTabId) {
            curTabId = tabId;
            bottomNavBar.selectTab(curTabId, false);
        }
        replaceFragment(fragment);
        getSupportFragmentManager().executePendingTransactions();
    }

    //volvemos al fragment principal del tab
    private void backToRoot() {
        if (isRootTabFragment(curFragment, curTabId)) {
            return;
        }
        resetBackStackToRoot(curTabId);
        Fragment rootFragment = popFragmentFromBackStack(curTabId);
        assert rootFragment != null;
        backTo(curTabId, rootFragment);
    }

    //preguntamos se es el fragment principal del tab
    private boolean isRootTabFragment(@NonNull Fragment fragment, int tabId) {
        return fragment.getClass() == rootTabFragment(tabId).getClass();
    }

    //metodo que reemplaza el fragment por el que le pasemos
    private void replaceFragment(@NonNull Fragment fragment) {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction tr = fm.beginTransaction();
        tr.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        tr.replace(R.id.content, fragment);
        tr.commitAllowingStateLoss();
        curFragment = fragment;
    }
}
