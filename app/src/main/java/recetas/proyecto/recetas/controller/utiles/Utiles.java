package recetas.proyecto.recetas.controller.utiles;

import java.util.ArrayList;

/**
 * Created by javi on 26/02/2018.
 */

public class Utiles {
    public static String arrayListIngredientesToString(ArrayList<?> list) {
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < list.size(); i++) {
            result.append(", ").append(list.get(i).toString());
        }
        return result.substring(2);
    }
}
