package recetas.proyecto.recetas.controller.compra;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetSequence;

import recetas.proyecto.recetas.MainActivity;
import recetas.proyecto.recetas.R;
import recetas.proyecto.recetas.controller.despensa.BuscarIngredienteFragment;
import recetas.proyecto.recetas.controller.despensa.DespensaAdapter;
import recetas.proyecto.recetas.model.constantes.SharedConstantes;

public class CompraFragment extends Fragment {
    //constante
    private static final String ARG_ITEM = "item";
    //item que le pasamos al momento de llamar al metodo newINstance
    private String item;

    //elementos de la vista
    private RecyclerView recyclerView;
    private DespensaAdapter iAdapter;
    private Button terminarDeComprar;
    private ConstraintLayout layoutNoHayIng;

    public CompraFragment() {
    }

    //llamaremos a este método statico para generar una nueva instancia de esta clase con los argumentos (item)
    public static CompraFragment newInstance(String item) {
        CompraFragment fragment = new CompraFragment();
        Bundle args = new Bundle();
        args.putString(ARG_ITEM, item);
        fragment.setArguments(args);
        return fragment;
    }

    //este método se ejecuta cuando se crea una nueva instancia del fragemnt, y se le asignan los argumentos (item) que hemos definido en el método anterior
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        item = getArguments().getString(ARG_ITEM);
    }

    //recuperamos los elementos de la vista y los configuramos
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //inflamos la vista, reguperamos los elementos, ponemos listener al botón, creamos un nuevo adapter y se lo asignamos al recyclerview
        View vista = inflater.inflate(R.layout.fragment_compra, container, false);

        recyclerView = (RecyclerView) vista.findViewById(R.id.recycler_view_compra);
        layoutNoHayIng = (ConstraintLayout) vista.findViewById(R.id.constraintNoHayIngListaCompra);

        terminarDeComprar = (Button)  vista.findViewById(R.id.terminarComprar);
        terminarDeComprar.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                eliminarYAnadirADespensa();
                ponFondoAlRecycler();
            }
        });

        //conseguimos la imagen de fondo que se mostrará solo si la lista está vacia
        ImageButton fondo = (ImageButton) vista.findViewById(R.id.add_food_to_your_list);
        fondo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).showFragment(BuscarIngredienteFragment.newInstance(getString(R.string.add_ingredients_list),1));
            }
        });


        iAdapter = new DespensaAdapter(((MainActivity)getActivity()).compraData.getListaIngredientes(),true);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(iAdapter);

        //conseguimos el shred preferences
        SharedPreferences prefs = vista.getContext().getSharedPreferences(SharedConstantes.nombre, Context.MODE_PRIVATE);
        //si el atributo tutorial del shared preferences es false, mostramos el tutorial (para que solo se muestre la primera vez)
        Boolean tutorialDone = prefs.getBoolean(SharedConstantes.tutorialLista, false);
        if (!tutorialDone) {
            new TapTargetSequence(getActivity())
                    .targets(  // Specify the target radius (in dp)
                            TapTarget.forView(vista.findViewById(R.id.add_food_to_your_list), getString(R.string.tutorialLista), getString(R.string.tutorialListaDescrip))
                                    // All options below are optional
                                    .outerCircleColor(R.color.colorAccent)      // Specify a color for the outer circle
                                    .outerCircleAlpha(0.96f)            // Specify the alpha amount for the outer circle
                                    .targetCircleColor(R.color.blanco)   // Specify a color for the target circle
                                    .titleTextSize(24)                  // Specify the size (in sp) of the title text
                                    .titleTextColor(R.color.blanco)      // Specify the color of the title text
                                    .descriptionTextSize(14)            // Specify the size (in sp) of the description text
                                    .descriptionTextColor(R.color.blanco)  // Specify the color of the description text
                                    .textColor(R.color.blanco)            // Specify a color for both the title and description text
                                    .drawShadow(true)                   // Whether to draw a drop shadow or not
                                    .cancelable(false)                  // Whether tapping outside the outer circle dismisses the view
                                    .tintTarget(false)                   // Whether to tint the target view's color
                                    .transparentTarget(true)           // Specify whether the target is transparent (displays the content underneath)// Specify a custom drawable to draw as the target
                                    .targetRadius(60)
                    ).start();
            SharedPreferences.Editor editor = vista.getContext().getSharedPreferences(SharedConstantes.nombre, Context.MODE_PRIVATE).edit();
            editor.putBoolean(SharedConstantes.tutorialLista, true); //ponemos el valor a false para que no se vulva a mostar
            editor.apply();
        }

        //llamamos a los siguientes metodos
        ponFondoAlRecycler();
        crearIngredientes();

        return vista;
    }

    //metodo que se ejecuta al pulsar el boton "terminar de comprar". Comprueba que los ingredientes no estén ya en la despensa y los elimina de la lista y lo añade a despensa.
    private void eliminarYAnadirADespensa(){
        boolean haySeleccionados = false;

        for (int i = ((MainActivity)getActivity()).compraData.getListaIngredientes().size()-1; i>=0;i--){
            if (((MainActivity)getActivity()).compraData.getListaIngredientes().get(i).isEstoySeleccionado()){
                haySeleccionados = true;
                if (!((MainActivity) getActivity()).despensaData.getListaIngredientes().contains(((MainActivity)getActivity()).compraData.getListaIngredientes().get(i))){
                    ((MainActivity) getActivity()).despensaData.getListaIngredientes().add(((MainActivity)getActivity()).compraData.getListaIngredientes().get(i));
                    ((MainActivity)getActivity()).compraData.getListaIngredientes().get(i).setEstoySeleccionado(false);
                    ((MainActivity)getActivity()).compraData.getListaIngredientes().remove(i);
                }
            }
            Toast.makeText(getContext(),getString(R.string.ingredients_added),Toast.LENGTH_SHORT).show();
        }
        if (!haySeleccionados){
            Toast.makeText(getContext(),getString(R.string.select_ingredients_to_pantry),Toast.LENGTH_SHORT).show();
        }
        iAdapter.notifyDataSetChanged();
    }

    //si no hay ingredientes para mostrar en el recycler view mostramos una imagen de fondo
    private void ponFondoAlRecycler(){
        if (((MainActivity)getActivity()).compraData.getListaIngredientes().size()==0){
            layoutNoHayIng.setVisibility(View.VISIBLE);
            terminarDeComprar.setVisibility(View.INVISIBLE);
        } else {
            layoutNoHayIng.setVisibility(View.INVISIBLE);
            terminarDeComprar.setVisibility(View.VISIBLE);
        }
    }


    private void crearIngredientes() {
        for (int i = 0; i< 2; i++){
           // listaIngredientes.add(new Ingrediente("Ingredient "+i,i+"","mg"));
        }
        iAdapter.notifyDataSetChanged();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setUpActionBar();

    }

    //conseguimos y configuramos la actionbar
    @SuppressWarnings("ConstantConditions")
    private void setUpActionBar() {
        ActionBar actionBar = ((MainActivity) getActivity()).getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(false);
        actionBar.setTitle(item);
    }

    //añadimos el men´ñu a la actionbar
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_add, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }
    //definimos las acciones de los menús
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                getActivity().onBackPressed();
                return true;
            case R.id.anadir:
                ((MainActivity) getActivity()).showFragment(BuscarIngredienteFragment.newInstance(getString(R.string.add_ingredients_list),1));
                return true;
            case R.id.delete:
                if (((MainActivity) getActivity()).compraData.getListaIngredientes().size()>0){
                    ((MainActivity) getActivity()).compraData.deleteIngredientesSeleccionados();
                    iAdapter.notifyDataSetChanged();
                    ponFondoAlRecycler();
                } else {
                    Toast.makeText(getContext(),getString(R.string.no_ingredients_to_remove),Toast.LENGTH_SHORT).show();
                }

                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}