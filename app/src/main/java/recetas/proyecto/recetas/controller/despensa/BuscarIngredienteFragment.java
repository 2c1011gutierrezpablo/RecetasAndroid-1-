package recetas.proyecto.recetas.controller.despensa;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.PorterDuff;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.Toast;


import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;

import me.sudar.zxingorient.ZxingOrient;
import me.sudar.zxingorient.ZxingOrientResult;
import recetas.proyecto.recetas.MainActivity;
import recetas.proyecto.recetas.R;
import recetas.proyecto.recetas.controller.utiles.Json;
import recetas.proyecto.recetas.model.despensa.Ingrediente;

public class BuscarIngredienteFragment extends Fragment {

    //constantes
    private static final String ARG_ITEM = "item";
    private static final String ARG_FUNCION = "funcion";

    //parametros para definir el titulo del fragemnt y la función. Depende del número de "función" los ingredientes se devolverán a un sitio u otro
    private String item;
    private int funcion; //0 despensa, 2 lista compra

    //lista de ingredientes
    private ArrayList<Ingrediente> listaIngredientes;

    //elemtos de la vista y adapter
    private EditText busquedaIngrediente;
    private DespensaAdapter adapter;
    private ProgressBar cargando;

    //url de la petición para mostrar ciertos ingredientes de serie en el fragemnt
    private String urlPeticion = "http://reasype.com/api/ingredientes.php?id=1,2,3,4,5,7";

    public BuscarIngredienteFragment() {
    }

    //constructor estatico al cual le pasamos los parametros deseados
    public static BuscarIngredienteFragment newInstance(String item, int funcion) {
        BuscarIngredienteFragment fragment = new BuscarIngredienteFragment();
        Bundle args = new Bundle();
        args.putString(ARG_ITEM, item);
        args.putInt(ARG_FUNCION, funcion);
        fragment.setArguments(args);
        return fragment;
    }

    //al crearse el fragemnt se le asignarán los argumentos que hemos definidos en el constructor estatico
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        item = getArguments().getString(ARG_ITEM);
        funcion = getArguments().getInt(ARG_FUNCION);
        listaIngredientes = new ArrayList<>();
    }

    //recuperamos los elementos de la vista y los configuramos
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //inflamos al vista con el XML, recuperamos los elementos, creamos el adapter y lo asignamos al recyclerview, y añadimos los ayentes correspondientes al campo de busqueda
        View vista = inflater.inflate(R.layout.fragment_buscar_ingrediente, container, false);

        RecyclerView recycler_view = (RecyclerView) vista.findViewById(R.id.recycler_buscar_ing);

        busquedaIngrediente = (EditText) vista.findViewById(R.id.busquedaIngrediente);

        busquedaIngrediente.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }
            //cuando el texto cambie en el cuadro de busqueda se ejecute este metodo
            @Override
            public void afterTextChanged(Editable editable) {

                if (editable.toString().trim().length()==0){
                } else { //si hay algo escrito, comprobamos si lo introducido son números o texto
                    if (isNumeric(editable.toString())){ //si son numeros, buscamos el codido de barras en openfoodfacts (que devolverá ingredientes parecidos de nuestra base de datos)
                        String urlPeticionTexto ="https://world.openfoodfacts.org/api/v0/product/"+editable.toString()+".json";
                        new PeticionAsincronaOpenFoodFacts().execute(urlPeticionTexto);
                    } else { //si es texto, buscamos los ingredientes en nuestra base de datos por nombre similar
                        String urlPeticionTexto ="http://reasype.com/api/ingredientesConNombreSimilar.php?nombre="+editable.toString();
                        new PeticionAsincrona().execute(urlPeticionTexto);
                    }

                }

            }
        });

        //al pulsar el botón "X" se borrará la barra de busqueda y los ingredientes que se muestran
        ImageButton borrarBusqueda = (ImageButton) vista.findViewById(R.id.borrarBusqueda);
        borrarBusqueda.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listaIngredientes.clear();
                adapter.notifyDataSetChanged();
                busquedaIngrediente.setText("");
            }
        });

        //recuperamos mas elemntos de la vista y configuramos nuestro recyclerview
        cargando = (ProgressBar) vista.findViewById(R.id.progessBuscarIng);
        recycler_view.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter = new DespensaAdapter(listaIngredientes,true);
        recycler_view.setAdapter(adapter);

        //conseguimos el boton que lanza el lector de codigo de barras y pedimos previamente los permisos
        ImageButton botonCamaraBarcode = (ImageButton) vista.findViewById(R.id.barcodeImg);
        botonCamaraBarcode.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String[] permissionArrays = new String[]{Manifest.permission.CAMERA};
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(permissionArrays, 11111);
                } else {
                    lanzaEscaner();
                }
            }
        });
        return vista;
    }

    //configuramos y lanzamos el intent con el lector de codigo de barras
    public void lanzaEscaner(){
        ZxingOrient integrator = new ZxingOrient(BuscarIngredienteFragment.this);
        integrator.setToolbarColor("#4caf50")       // Sets Tool bar Color
                .setInfoBoxColor("#4caf50")       // Sets Info box color
                .setInfo(getString(R.string.scan_barcode))
                .setBeep(true)  // Doesn't play beep sound
                .setVibration(true)// Sets info message in the info box
                .initiateScan();
    }

    //metodo que comprueba si un string es numerico
    public boolean isNumeric(String s) {
        return s != null && s.matches("[-+]?\\d*\\.?\\d+");
    }

    //metodo que se ejecuta cuando seleccionamos los permisos
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 11111: { //si el permiso de camara ha sifo concedido lanzamos el lector de codigo de barras, si no, mostramos un toast
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    lanzaEscaner();

                } else {
                    Toast.makeText(getContext(), getString(R.string.permisson_camera), Toast.LENGTH_SHORT).show();
                }
                return;
            }
        }
    }

    //Metodo que se ejecuta cuando el intent del lector de codigo de barras finaliza
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        ZxingOrientResult scanResult = ZxingOrient.parseActivityResult(requestCode, resultCode, data);

        if (scanResult != null) { //si el resultado  no es nulo
            if(scanResult.getContents() == null) { //si el contenido es nulo
                //Toast.makeText(getContext(), "Cancelled", Toast.LENGTH_LONG).showSelectorDePasos();
            } else { //si se ha conseguido escanear el codigo de barras cambiamos la busqueda por el codigo de barras
                busquedaIngrediente.setText(scanResult.getContents());
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }
    //al crearse este fragemnt lanzamos una petición asincrona apra mostrar los ingredientes predeterminados
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setUpActionBar();
        new PeticionAsincrona().execute(urlPeticion);
    }

    //configuramos la actionabr
    @SuppressWarnings("ConstantConditions")
    private void setUpActionBar() {
        ActionBar actionBar = ((MainActivity) getActivity()).getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.drawable.ic_close_black_24dp);
        actionBar.setTitle(item);
        //setHasOptionsMenu(true);
    }

    //inflamos el menu
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_done, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    //definimos las acciones de los menús
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                getActivity().onBackPressed();
                return true;
            case R.id.done:
                if (funcion==0) { //añadir a la despensa
                    for (int i = 0; i< listaIngredientes.size(); i++){
                        if (listaIngredientes.get(i).isEstoySeleccionado()){
                            listaIngredientes.get(i).setEstoySeleccionado(false);
                            if (!((MainActivity) getActivity()).despensaData.getListaIngredientes().contains(listaIngredientes.get(i))){
                                ((MainActivity) getActivity()).despensaData.getListaIngredientes().add(listaIngredientes.get(i));
                            }
                        }
                    }
                } else if(funcion==1){//añadir a la lista de la compra
                    for (int i = 0; i< listaIngredientes.size(); i++){
                        if (listaIngredientes.get(i).isEstoySeleccionado()){
                            listaIngredientes.get(i).setEstoySeleccionado(false);
                            if (!((MainActivity) getActivity()).compraData.getListaIngredientes().contains(listaIngredientes.get(i))){
                                ((MainActivity) getActivity()).compraData.getListaIngredientes().add(listaIngredientes.get(i));
                            }
                        }
                    }
                }else if(funcion == 2){//añadir a receta
                        for (int i = 0; i< listaIngredientes.size(); i++){
                            if (listaIngredientes.get(i).isEstoySeleccionado()){
                                listaIngredientes.get(i).setEstoySeleccionado(false);
                                if (!((MainActivity) getActivity()).recetasData.getListaIngredientesAñadir().contains(listaIngredientes.get(i))){
                                    ((MainActivity) getActivity()).recetasData.getListaIngredientesAñadir().add(listaIngredientes.get(i));
                                }
                            }
                        }
                }
                getActivity().onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    //Conseguimos el Json resultado de la peticón GET con el URL que le pasemos y en "onPostExecute" lo deserializamos y lso mostramos en el recyclerview
    protected class PeticionAsincrona extends AsyncTask<String, String, String> {

     @Override
        protected String doInBackground(String... strings) {
            return Json.leeJsonDesdeLaURL(strings[0]);
        }
        @Override
        protected void onPostExecute(String json) {
            Ingrediente[] ingredientes = new Gson().fromJson(json, Ingrediente[].class);
            if (ingredientes!=null){
                listaIngredientes.clear();
                listaIngredientes.addAll((Arrays.asList(ingredientes)));
                adapter.notifyDataSetChanged();
            }
            cargando.setVisibility(View.GONE);

        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            cargando.setMax(100);
            cargando.setVisibility(View.VISIBLE);
        }
    }
    //conseguimos el nombre del ingrediente mediante el codigo de barras ahciendo una petición a openfoodfacts. En "onPostExecute" conseguimos el nombre del producto y lo buscamos por similitud lanzando otra petición asincrona a nuestro servidor
    protected class PeticionAsincronaOpenFoodFacts extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String... strings) {
              return Json.leeJsonDesdeLaURL(strings[0]);
        }
        @Override
        protected void onPostExecute(String json) {
            System.out.println(json);
            try {
                JSONObject jsonOpenFoodFacts = new JSONObject(json);
                String nombreIng = jsonOpenFoodFacts.getJSONObject("product").getString("product_name");
                Toast.makeText(getContext(),nombreIng,Toast.LENGTH_SHORT).show();
                String urlPeticionTexto ="http://reasype.com/api/ingredientesConNombreSimilar.php?nombre="+nombreIng;
                new PeticionAsincrona().execute(urlPeticionTexto);
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            cargando.setMax(100);
            cargando.setVisibility(View.VISIBLE);
        }
    }

    



}
