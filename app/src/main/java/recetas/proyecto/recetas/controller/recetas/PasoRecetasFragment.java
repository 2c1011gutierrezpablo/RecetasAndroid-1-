package recetas.proyecto.recetas.controller.recetas;

import android.Manifest;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.speech.tts.TextToSpeech;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.arlib.floatingsearchview.util.Util;
import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Locale;

import recetas.proyecto.recetas.R;
import recetas.proyecto.recetas.MainActivity;
import recetas.proyecto.recetas.controller.utiles.Utiles;
import recetas.proyecto.recetas.model.constantes.Servidor;
import recetas.proyecto.recetas.model.recetas.Receta;


public class PasoRecetasFragment extends Fragment implements TextToSpeech.OnInitListener, RecognitionListener {

    //constante y valor de los argumentos que se usaran al construir este fragemnt
    private static final String ARG_ITEM = "item";
    private static final String ARG_RECETA = "receta";
    private String item;
    private Receta receta;

    private int contadorPasos = 0;//cuenta la posicion del paso a mostrar

    //elementos de la vista
    private TextView textoPaso;
    private ActionBar actionBar;
    private TextToSpeech engine;
    private MenuItem hablar;
    private Button escuchar;
    private ImageView imagenpaso;

    //recogoncedor de voz
    private SpeechRecognizer speech = null;
    private Intent recognizerIntent;
    private String LOG_TAG = "VoiceRecognitionActivity";

    public PasoRecetasFragment() {
    }

    //constructor estatico que devuelve una nueva instancia de esta clase con los argumentos que le apsamos
    public static PasoRecetasFragment newInstance(String item, Receta receta) {
        PasoRecetasFragment fragment = new PasoRecetasFragment();
        Bundle args = new Bundle();
        args.putString(ARG_ITEM, item);
        args.putSerializable(ARG_RECETA, (Serializable) receta);
        fragment.setArguments(args);
        return fragment;
    }
    //al crearse una instancia recuperamos los argumentos que hemos definido en el constructor estatico y los asignamos
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        item = getArguments().getString(ARG_ITEM);
        receta = (Receta) getArguments().getSerializable(ARG_RECETA);
        //inicializamos el lector de texto
        engine = new TextToSpeech(getContext(), this);
    }


    @Override //recuperacion de los datos de la vista y la configuramos
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //inflamos la vista con el XML correspondiente
        View vista = inflater.inflate(R.layout.fragment_paso_recetas, container, false);

        //Configuramos y asignamos las acciones correspondientes al reconocedor de VOZ
        speech = SpeechRecognizer.createSpeechRecognizer(getContext());
        Log.i(LOG_TAG, "isRecognitionAvailable: " + SpeechRecognizer.isRecognitionAvailable(getContext()));
        speech.setRecognitionListener(this);

        recognizerIntent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        recognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_PREFERENCE,
                "en");
        recognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        recognizerIntent.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, 3);

        textoPaso =(TextView)vista.findViewById(R.id.tPaso);
        imagenpaso  =(ImageView)vista.findViewById(R.id.imagen_paso);
        //descargamos la imagen con piccaso (libreria)
        Picasso.with(getContext()).load(Servidor.host+receta.getPasos().get(contadorPasos).getUrlImagen()).into(imagenpaso);

        escuchar = (Button)vista.findViewById(R.id.bEscucha);
        escuchar.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                speech.startListening(recognizerIntent);
                recognizerIntent.putExtra(RecognizerIntent.EXTRA_SPEECH_INPUT_POSSIBLY_COMPLETE_SILENCE_LENGTH_MILLIS, 2000);
            }
        });

        //conseguimos los elementos de la vista y les asociamos acciones al boton
        Button bFin = (Button) vista.findViewById(R.id.bFin);
        bFin.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
             finPaso();
            }
        });

        ImageButton bsigiente = (ImageButton) vista.findViewById(R.id.bSiguente);
        bsigiente.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                siguientePaso();
            }
        });

        ImageButton banterior = (ImageButton) vista.findViewById(R.id.bAnterior);
        banterior.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                anteriorPaso();
            }
        });

        return vista;
    }

    //este metodo se ejecuta despues de que la vista sea creada
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setUpActionBar();
        cargarMoverReceta(); //llamamos al metodo

    }
    //configuramos la actionbar
    @SuppressWarnings("ConstantConditions")
    private void setUpActionBar() {
        actionBar = ((MainActivity) getActivity()).getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp);
        actionBar.setTitle(item);
        setHasOptionsMenu(true);
    }
    //inflamos la vista con el menu correspondiente
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_voz, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    //definimos las acciones de los menus al ser pulsados
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                getActivity().onBackPressed();
                return true;
            case R.id.hablar:
                hablar = item;
                speakText(textoPaso,item); //cuando se pulse el botón de "hablar" se ejecuta el metodo correspondiente
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    //carga el paso correspondiente de la receta
    public void cargarMoverReceta(){
        if (contadorPasos<=receta.getPasos().size()){
           actionBar.setTitle(getString(R.string.step)+" "+(contadorPasos+1));
           textoPaso.setText(receta.getPasos().get(contadorPasos).getDescripcion());
            //descargamos la imagen con piccaso (libreria)
            Picasso.with(getContext()).load(Servidor.host+receta.getPasos().get(contadorPasos).getUrlImagen()).into(imagenpaso);
        }
    }

    //metodo que se ejecuta al pulsar el boton siguiente, cambiamos el contador de pasos y ejecutamos cargarmoverreceta
    public boolean siguientePaso(){
        if (contadorPasos<receta.getPasos().size()-1){
           contadorPasos++;
           cargarMoverReceta();
           return true;
        }
        return false;
    }

    //metodo que se ejecuta al pulsar el boton siguiente, cambiamos el contador de pasos y ejecutamos cargarmoverreceta
    public boolean anteriorPaso(){
        if (contadorPasos>0){
            contadorPasos--;
            cargarMoverReceta();
            return true;
        }
        return false;
    }

    //al pulsar fin volvemos al fragment anterior (esto lo gistiona el mainactivity)
    public void finPaso(){
        //eliminamos los ingredientes utilizados de la despensa
        for (int i = 0; i< receta.getIngredientes().size(); i++){
            ((MainActivity) getActivity()).despensaData.getListaIngredientes().remove(receta.getIngredientes().get(i));
        }
        Toast.makeText(getContext(), Utiles.arrayListIngredientesToString(receta.getIngredientes())+" "+getString(R.string.removed_from_pantry),Toast.LENGTH_SHORT).show();

        ((MainActivity) getActivity()).onBackPressed();
    }

    //metodo que lee el campo "textoPaso" comprobando la versión de nuestro dispositivo para saber que método es el adecuado para lanzar
    public void speakText(View v, MenuItem item) {
        if (!engine.isSpeaking()){
            String textContents = textoPaso.getText().toString();
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                engine.speak(textContents, TextToSpeech.QUEUE_FLUSH, null, null);
            } else {
                engine.speak(textContents, TextToSpeech.QUEUE_FLUSH, null);
            }
        } else { //si el tts ya está leyendo, lo paramos
           engine.stop();
        }

    }
    //metodo que inizializa y configura el texttospech
    @Override
    public void onInit(int i) {
        if (i == TextToSpeech.SUCCESS) {
            Locale spanish = new Locale("en");
            engine.setLanguage(spanish);
            engine.setPitch(1);
        }
    }

    @Override
    public void onResume() { //este metodo gestiona las pulsaciones, si se presiona la tecla atrás, en vez de cerrar el fragment carga el paso anterior
        super.onResume();
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK){
                    if (!anteriorPaso()){
                        ((MainActivity) getActivity()).onBackPressed();
                    }
                    return true;
                }
                return false;
            }
        });
    }

    //todos los metoso aprtir son relacionados con el reconocimiento de voz

    //se llama en el moemnto que se comienza a hablar y ejecuta en ese momento lo que se indique

    @Override
    public void onReadyForSpeech(Bundle bundle) {


    }

    //cuando el usuario empieza a hablar se inicia este metodo y ejecuta en ese momento lo que se indique

    @Override
    public void onBeginningOfSpeech() {
        Log.i(LOG_TAG, "onBeginningOfSpeech");
    }

    //mide el nivel de sonido del audio y ejecuta en ese momento lo que se indique

    @Override
    public void onRmsChanged(float rmsdB) {

        Log.i(LOG_TAG, "onRmsChanged: " + rmsdB);



    }

    //este metodo almacena las voces secuandarias y ejecuta en ese momento lo que se indique

    @Override
    public void onBufferReceived(byte[] buffer) {

        Log.i(LOG_TAG, "onBufferReceived: " + buffer);




    }

    //este metodo se lanza cuando se termina de hablar y ejecuta en ese momento lo que se indique

    @Override
    public void onEndOfSpeech() {

        Log.i(LOG_TAG, "onEndOfSpeech");




    }

    //se ejecuta normalemnte cuando no hay conexion a la red y ejecuta en ese momento lo que se indique

    @Override
    public void onError(int i) {


    }

    //se ejecuta cuando los datos del reconcimiento estan listos

    @Override
    public void onResults(Bundle results) {

        Log.i(LOG_TAG, "onResults");
        ArrayList<String> matches = results
                .getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
        String text = "";
        for (String result : matches){

            switch (result){

                case "siguiente":
                    siguientePaso();
                    break;
                case "fin":
                    finPaso();
                    break;
                case "anterior":
                    anteriorPaso();
                    break;

            }


        }

    }

    //se ejecuta en le moemnto que se encuentran resultados parciales

    @Override
    public void onPartialResults(Bundle bundle) {

        Log.i(LOG_TAG, "onPartialResults");

            // WARNING: The following is specific to Google Voice Search
            String[] results = bundle.getStringArray("com.google.android.voicesearch.UNSUPPORTED_PARTIAL_RESULTS");
            System.out.println(bundle);



    }

    //este metodo esta reservado para agregar eventos futuros

    @Override
    public void onEvent(int i, Bundle bundle) {

        Log.i(LOG_TAG, "onEvent");

    }




}