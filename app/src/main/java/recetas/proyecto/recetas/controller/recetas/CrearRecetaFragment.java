package recetas.proyecto.recetas.controller.recetas;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;


import recetas.proyecto.recetas.MainActivity;
import recetas.proyecto.recetas.R;
import recetas.proyecto.recetas.controller.utiles.Post;
import recetas.proyecto.recetas.controller.despensa.BuscarIngredienteFragment;
import recetas.proyecto.recetas.model.despensa.Ingrediente;
import recetas.proyecto.recetas.model.recetas.Paso;
import recetas.proyecto.recetas.model.recetas.Receta;

public class CrearRecetaFragment extends Fragment{

    //constantes
    private static final String ARG_ITEM = "item";

    //parametros para definir el titulo del fragemnt y la función.
    //Tambien estan los diferentes elementos de la vista
    private String item;
    private Button buttonNumeroDePasos;
    private ImageView imagenReceta;
    private LinearLayout parentLayout;
    private TextView textoIngredientes, nombreReceta, descripcionReceta;
    private static Bitmap bImagenReceta = null;
    private Receta nuevaReceta = null;
    private ProgressBar cargando;
    private static int numeroDePasosAMostrar ;

    //es la variable que devolveremos al crear la vista
    private View vista;

    //constantes
    protected static final int CAMERA_REQUEST = 0;
    protected static final int GALLERY_PICTURE = 1;

    //son las diferentes listas que se usaran para la creaccion de una receta
    private ArrayList<LinearLayout> contenedorDePasos;
    private ArrayList<Paso> listDePasosCreados;
    private ArrayList<Ingrediente> listaIngredientes;



    public CrearRecetaFragment() {
    }

    //constructor estatico al cual le pasamos los parametros deseados
    public static CrearRecetaFragment newInstance(String item) {
        CrearRecetaFragment fragment = new CrearRecetaFragment();
        Bundle args = new Bundle();
        args.putString(ARG_ITEM, item);
        fragment.setArguments(args);
        return fragment;
    }

    //al crearse el fragemnt se le asignarán los argumentos que hemos definidos en el constructor estatico
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        item = getArguments().getString(ARG_ITEM);
        listaIngredientes = ((MainActivity)getActivity()).recetasData.getListaIngredientesAñadir();
        ((MainActivity) getActivity()).recetasData.borrarlistaIngredientesAñadir();
    }


    //recuperamos los elementos de la vista y los configuramos
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //inflamos al vista con el XML,
        // recuperamos los elementos, creamos el adapter y lo asignamos al recyclerview

        vista = inflater.inflate(R.layout.fragment_crear_receta, container, false);


        //carga la lista de pasos la cual contendra el contender de los pasos a mostrar
        cargaListaPasos();


        //likamos el campo nombre, la descripcion, el boton para mostrar los pasos,
        // el campo donde se almacenaran los ingredeintes añadisos

        nombreReceta = (EditText)vista.findViewById(R.id.eNombre);
        descripcionReceta = (EditText)vista.findViewById(R.id.eDescripcion);
        buttonNumeroDePasos =(Button)vista.findViewById(R.id.bNumeros);
        textoIngredientes = (TextView) vista.findViewById(R.id.tIngredientesAdd);



        //se ocultan todos los pasos para que el cliente elija cuantos tendera su receta
        mostrarIngredeientesAñadidos();


        //linkeamos el campo imagen al que despues le asocianremos una accion para asiganr la imagen

        imagenReceta=(ImageView) vista.findViewById(R.id.imagen_add_receta);

        if (bImagenReceta !=null)
            imagenReceta.setImageBitmap(bImagenReceta);

        imagenReceta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //creamos un alert el cual sacara dos botones camara y galeria

                AlertDialog.Builder myAlertDialog = new AlertDialog.Builder(
                        getActivity());
                myAlertDialog.setTitle("Upload Pictures Option");
                myAlertDialog.setMessage("How do you want to set your picture?");

                //al presionar galery  abrira la galeria y podras seleccionar la foto

                myAlertDialog.setPositiveButton("Gallery",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface arg0, int arg1) {


                                Intent pictureActionIntent = null;

                                pictureActionIntent = new Intent(
                                        Intent.ACTION_PICK,
                                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                startActivityForResult(
                                        pictureActionIntent,
                                        GALLERY_PICTURE);

                            }
                        });

                //al presionar camra se devera de abrir la camara y luego podras hacer una foto la cual se guardara

               /*myAlertDialog.setNegativeButton("Camera",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface arg0, int arg1) {

                                Intent intent = new Intent(
                                        MediaStore.ACTION_IMAGE_CAPTURE);
                                File f = new File(android.os.Environment
                                        .getExternalStorageDirectory(), "temp.jpg");
                                intent.putExtra(MediaStore.EXTRA_OUTPUT,
                                        Uri.fromFile(f));

                                startActivityForResult(intent,
                                        CAMERA_REQUEST);

                            }
                        });
                */

                myAlertDialog.show();



            }
        });

        //mostrara los ingredientes que contendra la receta
        ponEnVisibleElLosPasosSelecionados(numeroDePasosAMostrar);

        //asiganamos al boton de añadir ingrediente la accion de cargar el buscardor de ingredientes
        Button addIngrediente  = (Button)vista.findViewById(R.id.bRecetatoIngrediente);
        addIngrediente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity) getActivity()).showFragment(BuscarIngredienteFragment.newInstance(getString(R.string.add_ingredients_pantry),2));

            }
        });

        //linkeamos el lineal layout  para luego usarlo y le asignamos al boton que despliege el selector de pasos

        parentLayout = (LinearLayout)vista.findViewById(R.id.linealLY);
        buttonNumeroDePasos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showSelectorDePasos();
            }
        });//abre dialo con selectro de listDePasosCreados

        //creamos la barra de carga del post
        cargando = (ProgressBar)vista.findViewById(R.id.pCarga);
        cargando.setVisibility(View.GONE);

        return vista;
    }

    //Se encarga de cargar la lista de pasos

    private void cargaListaPasos(){

        //inicamos la lista de layaut que contendra los diferentes pasos de la receta

        contenedorDePasos = new ArrayList<>();
        contenedorDePasos.add((LinearLayout) vista.findViewById(R.id.paso1));
        contenedorDePasos.add((LinearLayout) vista.findViewById(R.id.paso2));
        contenedorDePasos.add((LinearLayout) vista.findViewById(R.id.paso3));
        contenedorDePasos.add((LinearLayout) vista.findViewById(R.id.paso4));
        contenedorDePasos.add((LinearLayout) vista.findViewById(R.id.paso5));
        contenedorDePasos.add((LinearLayout) vista.findViewById(R.id.paso6));
        contenedorDePasos.add((LinearLayout) vista.findViewById(R.id.paso7));
        contenedorDePasos.add((LinearLayout) vista.findViewById(R.id.paso8));
        contenedorDePasos.add((LinearLayout) vista.findViewById(R.id.paso9));
        contenedorDePasos.add((LinearLayout) vista.findViewById(R.id.paso10));

    }

    //este metodo se encargara de mostrar los ingredientes que contiene la receta

    private void mostrarIngredeientesAñadidos(){


        if (listaIngredientes.size()>0) {
            textoIngredientes.setText("");
            for (Ingrediente i : listaIngredientes) {

                textoIngredientes.append(i.getNombre() + "\n");

            }
        }
    }

    //este metodo se encarga de ocultar los pasaso, ya que pone a gone el conetnedor de cada paso, y con ello se consigue que
    //el lelemto este pero no se pinte sobre la vista

    private void ocultarPasos(){

        for(int i = 0; i< contenedorDePasos.size(); i++) {

            contenedorDePasos.get(i).setVisibility(View.GONE);
        }

    }

    //Metodo que se ejecuta cuando se abre el item de camara y galerua

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(true) {


            Uri selectedImage = data.getData();

            Bitmap bitmap = null;

            try {
                bitmap = MediaStore.Images.Media.getBitmap(getContext().getContentResolver(), selectedImage);
                imagenReceta.setImageBitmap(bitmap);

                bImagenReceta = bitmap;
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    //se ponen a visisles los listDePasosCreados

    protected void ponEnVisibleElLosPasosSelecionados(int numeroPasos) {

       //si oculta antes de mostrarse por si la cantidas es inferior
        ocultarPasos();

        for(int i=0; i<numeroPasos; i++) {

            contenedorDePasos.get(i).setVisibility(View.VISIBLE);

        }


    }

    //al iniciar el fragemtn aqueallas acciones que se encuentren en este metodo se mostraran

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setUpActionBar();

    }


    //configuaramos las aaciones

    @SuppressWarnings("ConstantConditions")
    private void setUpActionBar() {
        ActionBar actionBar = ((MainActivity) getActivity()).getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.drawable.ic_close_black_24dp);
        actionBar.setTitle(item);
        //setHasOptionsMenu(true);
    }

    //inflamos el menu

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_done, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    //definimos las aaciones en el menu

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                getActivity().onBackPressed();
                return true;
            case R.id.done:

               nuevaReceta = creaReceta();
                if (nuevaReceta!=null) {

                    new CrearPost().execute();
                    // Post.sendPostReceta(r);

                    return true;
                }
        }
        return super.onOptionsItemSelected(item);
    }

    //Creamos la receta con la informacion que se a intrucido

    private Receta creaReceta() {

        crePasos();

        if (puedoCrearReceta()) {

            Receta nuevaReceta = new Receta(
                    0, "", nombreReceta.getText().toString(), descripcionReceta.getText().toString(), listDePasosCreados, listaIngredientes);

            return nuevaReceta;
        }else {

            AlertDialog alertDialog = new AlertDialog.Builder(getContext()).create();
            alertDialog.setTitle("Error");
            alertDialog.setMessage("Could not create the recipe, \n check that everything is fine");
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });

            alertDialog.show();

        }
        return null;
    }


    //se encarga de crear los pasos

    private void crePasos(){

        //inicio el array
        listDePasosCreados = new ArrayList<>();

        //recorro los contenedores
        for (int i = 0; i < contenedorDePasos.size(); i++) {

            //compurebo que hay texto
            if (contenedorDePasos.get(i).getVisibility() == View.VISIBLE) {

                LinearLayout layout = contenedorDePasos.get(i);

                //se extrae del padre los elementos que cuaelgan del
                View v = layout.getChildAt(1);

                EditText texto = (EditText) v;//se castea el elemento 2 a edit text
                Paso nuevoPaso = new Paso(i, texto.getText().toString(), "");
                listDePasosCreados.add(nuevoPaso); //se añade a la lista el texto del edit text
            }

        }

    }

    //se muestra el alert de seleccion de listDePasosCreados a mostrar

    public void showSelectorDePasos() {

        //creamos el piker
        final NumberPicker picker = new NumberPicker(getContext());
        picker.setMinValue(1);
        picker.setMaxValue(10);

        //le asiganmos un layout
        final FrameLayout layout = new FrameLayout(getContext());
        layout.addView(picker, new FrameLayout.LayoutParams(
                FrameLayout.LayoutParams.WRAP_CONTENT,
                FrameLayout.LayoutParams.WRAP_CONTENT,
                Gravity.CENTER));

        //creamos el alert y le asignamos las accion
        new AlertDialog.Builder(
                getContext()
        )
                .setView(layout)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        // do something with picker.getValue()
                    ponEnVisibleElLosPasosSelecionados(picker.getValue());
                    numeroDePasosAMostrar = picker.getValue();
                    }
                })
                .setNegativeButton(android.R.string.cancel, null)
                .show();



    }


    private Boolean puedoCrearReceta(){

        Boolean respuesta = false;

        if (!nombreReceta.getText().toString().equalsIgnoreCase("")){

            if (!descripcionReceta.getText().toString().equalsIgnoreCase("")){

                if (listaIngredientes.size()>0){

                    if(bImagenReceta !=null){

                        for (Paso i : listDePasosCreados){

                            if (!i.getDescripcion().equalsIgnoreCase("")){

                                respuesta = true;

                            }

                        }

                    }

                }

            }

        }

        return respuesta;
    }

    protected class CrearPost extends AsyncTask<String, String, String> {

        @Override
        protected void onPostExecute(String json) {

            getActivity().onBackPressed();
            Toast.makeText(getContext(), "Receta añadida", Toast.LENGTH_SHORT).show();

        }

        @Override
        protected String doInBackground(String... strings) {

            try {
                Post.sendPostReceta(nuevaReceta,bImagenReceta);

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            cargando.setMax(100);
            cargando.setVisibility(View.VISIBLE);
        }
    }



}