package recetas.proyecto.recetas.controller.recetas;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ProgressBar;


import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetSequence;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;

import recetas.proyecto.recetas.MainActivity;
import recetas.proyecto.recetas.R;
import recetas.proyecto.recetas.controller.despensa.BuscarIngredienteFragment;
import recetas.proyecto.recetas.controller.utiles.Json;
import recetas.proyecto.recetas.model.constantes.SharedConstantes;
import recetas.proyecto.recetas.model.despensa.Ingrediente;
import recetas.proyecto.recetas.model.recetas.Receta;

public class RecetasFragment extends Fragment {
    //constante y valor de los argumentos que se usaran al construir este fragemnt
    private static final String ARG_ITEM = "item";
    private String item;

    //el layot que contiene la imagen para cuando la lista está vacía
    private ConstraintLayout layoutNoHayIng;

    //las recetas que vamos a mostrar en la recyclerview
    private ArrayList<Receta> recetasAMostrar;

    //adapter del rexyclerview
    private RecetaAdapter adapter;

    //barra de cargando
    private ProgressBar cargando;

    //url al cual haremos la petición para obtener las recetas disponibles
    private final String urlPeticion = "http://reasype.com/api/recetasDisponiblesConIng.php?ing=";

    public RecetasFragment() {
    }

    //constructor estático que devulve una instancia del fragment
    public static RecetasFragment newInstance(String item) {
        RecetasFragment fragment = new RecetasFragment();
        Bundle args = new Bundle();
        args.putString(ARG_ITEM, item);
        fragment.setArguments(args);
        return fragment;
    }

    //se ejecuta al crearse una nueva instacia, recuperamos los argumentos y los asignamos
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        item = getArguments().getString(ARG_ITEM);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        //inicializamos la arraylist de las recetas disponibles
        recetasAMostrar = new ArrayList<>();

        //inflamos el layour con el XML correspondiente
        View vista = inflater.inflate(R.layout.fragment_recetas, container, false);

        //recuperamos los elementos de la vista
        layoutNoHayIng = (ConstraintLayout) vista.findViewById(R.id.constraintNoHayRecetas);

        //conseguimos la imagen de fondo que se mostrará solo si la lista está vacia
        ImageButton fondo = (ImageButton) vista.findViewById(R.id.add_food_to_your_pantry_recetas);
        fondo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).showFragment(BuscarIngredienteFragment.newInstance(getString(R.string.add_ingredients_pantry),0));
            }
        });

        cargando = (ProgressBar) vista.findViewById(R.id.progessCargarRecetas);

        //recuperamos y configuramos el recyclerview
        RecyclerView recycler_view = (RecyclerView) vista.findViewById(R.id.recycler_view);
        recycler_view.setLayoutManager(new LinearLayoutManager(getContext()));

        //creamos un nuevo adapter personalizado y se lo asignamos a nuestro recycler
        adapter = new RecetaAdapter(recetasAMostrar, getContext());
        recycler_view.setAdapter(adapter);

        //conseguimos el shred preferences
        SharedPreferences prefs = vista.getContext().getSharedPreferences(SharedConstantes.nombre, Context.MODE_PRIVATE);
        //si el atributo tutoriala del shared preferences es false, mostramos el tutorial (para que solo se muestre la primera vez)
        Boolean tutorialDone = prefs.getBoolean(SharedConstantes.tutorialRecetas, false);
        if (!tutorialDone) {
            new TapTargetSequence(getActivity())
                    .targets(  // Specify the target radius (in dp)
                            TapTarget.forView(vista.findViewById(R.id.add_food_to_your_pantry_recetas), getString(R.string.tutorialReceta), getString(R.string.tutorialRecetaDescrip))
                                    // All options below are optional
                                    .outerCircleColor(R.color.colorAccent)      // Specify a color for the outer circle
                                    .outerCircleAlpha(0.96f)            // Specify the alpha amount for the outer circle
                                    .targetCircleColor(R.color.blanco)   // Specify a color for the target circle
                                    .titleTextSize(24)                  // Specify the size (in sp) of the title text
                                    .titleTextColor(R.color.blanco)      // Specify the color of the title text
                                    .descriptionTextSize(14)            // Specify the size (in sp) of the description text
                                    .descriptionTextColor(R.color.blanco)  // Specify the color of the description text
                                    .textColor(R.color.blanco)            // Specify a color for both the title and description text
                                    .drawShadow(true)                   // Whether to draw a drop shadow or not
                                    .cancelable(false)                  // Whether tapping outside the outer circle dismisses the view
                                    .tintTarget(false)                   // Whether to tint the target view's color
                                    .transparentTarget(true)           // Specify whether the target is transparent (displays the content underneath)// Specify a custom drawable to draw as the target
                                    .targetRadius(60)
                    ).start();
            SharedPreferences.Editor editor = vista.getContext().getSharedPreferences(SharedConstantes.nombre, Context.MODE_PRIVATE).edit();
            editor.putBoolean(SharedConstantes.tutorialRecetas, true); //ponemos el valor a false para que no se vulva a mostar
            editor.apply();
        }

        return vista;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setUpActionBar();
        //ejecutamos la petición para que nos carge la lista de recetas a mostrar
        if (recetasAMostrar.size()==0){
            new PeticionAsincrona().execute(urlPeticion);
        } else {

        }
    }

    //este metodo pone una imagen de fondo si la lista esta vacia
    private void ponFondoAlRecycler(){
        if (recetasAMostrar.size()==0){
            layoutNoHayIng.setVisibility(View.VISIBLE);
        } else {
            layoutNoHayIng.setVisibility(View.INVISIBLE);
        }
    }

    //configuramos la actionbar
    @SuppressWarnings("ConstantConditions")
    private void setUpActionBar() {
        ActionBar actionBar = ((MainActivity) getActivity()).getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(false);
        actionBar.setTitle(item);
    }

    //inflamos el menu
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_recetas_fragment, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    //definimos als acciones del menu
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                getActivity().onBackPressed();
                return true;
            case R.id.add_recipe: //si se pulsa add recipe, mostramos el fragment correspondiente para añadir una receta nueva
                ((MainActivity) getActivity()).showFragment(CrearRecetaFragment.newInstance(getString(R.string.create_recipe)));
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    //metodo que guarda el estado para cuando el fragment se desinstancie
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    //peticion asincrona a nuestro servidor que devulve las recetas que podemos hacer con los ingredientes que tenemos en nuestra despensa
    protected class PeticionAsincrona extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... strings) {
                String peticion = "";
                ArrayList<Ingrediente> listaIngredientesDespensa = ((MainActivity) getActivity()).despensaData.getListaIngredientes();
                if (listaIngredientesDespensa.size()!=0) {

                    for (int i = 0;i<listaIngredientesDespensa.size();i++){ //recuperamos los ingredientes de nuestra despensa y los añadimos a la url de la peticon
                        peticion+=","+listaIngredientesDespensa.get(i).getId();
                    }
                    peticion = strings[0]+peticion.substring(1);

                    return Json.leeJsonDesdeLaURL(peticion);
                }
            return null;
        }
        @Override
        protected void onPostExecute(String json) { //recuperamos el json y lo trnasformamos a nuestro objeto "receta" para mostrarlas en el recyclerview
            Receta[] recetas = new Gson().fromJson(json, Receta[].class);
            if (recetas!=null){
                recetasAMostrar.clear();
                recetasAMostrar.addAll((Arrays.asList(recetas)));
                adapter.notifyDataSetChanged();
            }
            cargando.setVisibility(View.GONE);
            ponFondoAlRecycler();
        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            cargando.setMax(100);
            cargando.setVisibility(View.VISIBLE);
        }
    }

}