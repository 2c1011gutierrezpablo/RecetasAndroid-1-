package recetas.proyecto.recetas.controller.despensa;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import recetas.proyecto.recetas.MainActivity;
import recetas.proyecto.recetas.R;
import recetas.proyecto.recetas.model.constantes.Servidor;
import recetas.proyecto.recetas.model.despensa.Ingrediente;

public class DespensaAdapter extends RecyclerView.Adapter<DespensaAdapter.MyViewHolder> {

    //declaramos nuestra lista de ingredientes
    private List<Ingrediente> ingredienteList;
    private boolean mostrarCheck;

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {

        //declaramos los elementos de la vista
        public TextView nombre;
        public Button cantidad;
        public ImageButton imagenIng;
        public CheckBox seleccionado;

        public View v;

        public MyViewHolder(View view) {
            super(view);
            v = view;
            //hacemos esta clase oyente de click y longclick
            view.setOnClickListener(this);
            //conseguimos los elementos de la vista
            nombre = (TextView) view.findViewById(R.id.nombre);
            cantidad = (Button) view.findViewById(R.id.cantidad);
            imagenIng = (ImageButton) view.findViewById(R.id.imagenIng);
            if (mostrarCheck){
                view.setOnLongClickListener(this);
                seleccionado = (CheckBox) view.findViewById(R.id.seleccionado);
            }
        }

        @Override
        public void onClick(View v) {
            if (mostrarCheck){
                seleccionaItemEnPos(v);
            } else {
                //al hacer click sobre un item lanzamos el detalle de este
                ((MainActivity) v.getContext()).showFragment(IngredienteDetalleFragment.newInstance(v.getContext().getString(R.string.detail),ingredienteList.get(getLayoutPosition())));
            }
        }

        @Override
        public boolean onLongClick(View view) {
            //al hacer long press sobre un item marcamos o desmarcamos el check
            seleccionaItemEnPos(view);
            return true;
        }

        private void seleccionaItemEnPos(View v){
            if (seleccionado.isChecked()){
                ingredienteList.get(getLayoutPosition()).setEstoySeleccionado(false);
            } else {
                ingredienteList.get(getLayoutPosition()).setEstoySeleccionado(true);
            }
            notifyItemChanged(getLayoutPosition());
        }
    }


    public DespensaAdapter(List<Ingrediente> ingredienteList,boolean mostrarCheck) {
        //asignamos nuestra lista de ingredientes al adapter
        this.ingredienteList = ingredienteList;
        this.mostrarCheck  = mostrarCheck;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //inflamos la vista del adapter con nuestro XML personalizado
        int vista;
        if (mostrarCheck){
            vista = R.layout.adapter_ingredientes_check;
        } else {
            vista = R.layout.adapter_ingredientes_despensa;
        }
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(vista, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        //conseguimos el ingrediente de una posición en concreto
        final Ingrediente ingrediente = ingredienteList.get(position);

        //asignamos los valores del ingrediente a los items de la vista
        holder.nombre.setText(ingrediente.getNombre());

        //si el ingrediente tiene cantidad la añadimos, si no, mostramos un mensaje por defecto
        if (ingrediente.getCantidad()!=0){
            holder.cantidad.setText(ingrediente.getCantidad()+" "+ingrediente.getUnidad());
        } else {
            holder.cantidad.setText(holder.v.getContext().getString(R.string.qty));
        }

        if (mostrarCheck){
            //ponemos el check a seleccionado o deseleccionado
            holder.seleccionado.setChecked(ingrediente.isEstoySeleccionado());
            holder.seleccionado.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    ingrediente.setEstoySeleccionado(b);
                }
            });
        }

        //añadimos un onLickListener al botón de nuestra vista
        holder.cantidad.setOnClickListener(new View.OnClickListener() {
            public void onClick(final View v) {
                //Al hacer click construimos un alertdialog con un edittext numérico
                AlertDialog.Builder alert = new AlertDialog.Builder(v.getContext());
                alert.setTitle(holder.v.getContext().getString(R.string.qty)+" "+ingrediente.getUnidad());
                final EditText input = new EditText(v.getContext());
                input.setInputType(InputType.TYPE_CLASS_NUMBER);
                input.setRawInputType(Configuration.KEYBOARD_12KEY);
                alert.setView(input);
                alert.setIcon(android.R.drawable.ic_dialog_info);
                alert.setPositiveButton(holder.v.getContext().getString(R.string.ok_text), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        //conseguimos el texto introducido en el edittext del alert
                        String texto = input.getText().toString().trim();
                        Float cant = -1f;
                        //si lo introducino no está vacío hacemos un parse
                        if (!texto.isEmpty()) {
                            cant = Float.parseFloat(texto);
                        }
                        //si el parse no ha fallado asignamos la nueva cantidad al ingrediente y actualizamos los cambios en la vista
                        if (cant != 0){
                            ingrediente.setCantidad(cant);
                            notifyItemChanged(holder.getAdapterPosition());
                        }
                    }
                });
                alert.setNegativeButton(holder.v.getContext().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        //cosas que hacer cuando el usuario haga click en cancelar
                    }
                });
                alert.show();
            }
        });

        holder.imagenIng.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity) holder.v.getContext()).showFragment(IngredienteDetalleFragment.newInstance(holder.v.getContext().getString(R.string.detail),ingredienteList.get(holder.getAdapterPosition())));
            }
        });

        //cargamos la imagen del ingrediente mediante la libreria piccaso, que descarga y cachea la imagen con el url deseado
        Picasso.with(holder.v.getContext()).load(Servidor.host+ingrediente.getUrlImagen()).into(holder.imagenIng);

    }

    @Override
    public int getItemCount() {
        return ingredienteList.size();
    }
}