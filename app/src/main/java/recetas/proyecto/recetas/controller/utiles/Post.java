package recetas.proyecto.recetas.controller.utiles;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.StrictMode;
import android.util.Base64;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLEncoder;

import javax.net.ssl.HttpsURLConnection;

import recetas.proyecto.recetas.model.despensa.Ingrediente;
import recetas.proyecto.recetas.model.recetas.Paso;
import recetas.proyecto.recetas.model.recetas.Receta;

/**
 * Created by pablogutierrez on 3/3/18.
 */

public class Post {


    //al invocar el metodo pasandole una receta se encargara de hacer un post en el servidor

    public static void sendPostReceta(Receta r, Bitmap img) throws UnsupportedEncodingException {


        //primero suvira al servidror la receta, el servidor devolvera el id de recerta el cual se almacenara en una variable
        //se pasara la url, el tipo de suvida y los datos en formato query, sera igual para los demas metodos llamados

        String idReceta= sendPost("https://reasype.com/recetas","application/x-www-form-urlencoded",getQueryReceta(r,img));

        //despues suvieremos los diferentes pasos de la receta

        for (int i = 0 ; i<r.getPasos().size(); i++){

            sendPost("https://reasype.com/pasos","application/x-www-form-urlencoded",
                    getQueryRecetaPasos(r.getPasos().get(i),idReceta));

        }

        //una vez suvida suvido lo anterior suvieremos los diferentes ingredietnes de los que dispone la receta

        for (int i = 0 ; i<r.getIngredientes().size(); i++){

            sendPost("https://reasype.com/ingredientes_cantidad_recetas","application/x-www-form-urlencoded",
                    getQueryRecetaIngrediente(r.getIngredientes().get(i),idReceta));

        }


    }

    //al invocar el metodo pasandole una imagen en bitmap se encargara de hacer la peticion post en el servidor para suvir la imagen pasada

    public static String sendPostImagen(Bitmap img) throws UnsupportedEncodingException {

        //primero suvira al servidror la imagen, el servidor devolvera la ruta de la receta
        //se pasara la url, el tipo de suvida y los datos en formato query y devolvera la ruta en laque se guardara
        String rutaImagen = sendPost("http://reasype.com/api/subeImagen.php","multipart/form-data",getQuerySubeImagen(img));
        return rutaImagen;
    }

    //este metodo crea los diferentes parametrso que tendra la receta para poder realizar el post

    private static String getQueryReceta(Receta r,Bitmap image) throws UnsupportedEncodingException {
        StringBuilder result = new StringBuilder();
        boolean first = true;

        //creamos el query para enviar el post
        result.append("&");

        result.append(URLEncoder.encode("nombre", "UTF-8"));
        result.append("=");
        result.append(URLEncoder.encode(r.getNombre(), "UTF-8"));

        result.append("&");

        result.append(URLEncoder.encode("descripcion", "UTF-8"));
        result.append("=");
        result.append(URLEncoder.encode(r.getDescripcion(), "UTF-8"));

        result.append("&");

        result.append(URLEncoder.encode("urlImagen", "UTF-8"));
        result.append("=");
        result.append(URLEncoder.encode(sendPostImagen(image), "UTF-8"));


        //devolvemos los datos formateados para poder ser enviados en el post
        return result.toString();
    }

    //este metodo crea los diferentes parametrso que tendra el paso de la receta que se pase como id

    private static String getQueryRecetaPasos(Paso r, String idReceta) throws UnsupportedEncodingException {
        StringBuilder result = new StringBuilder();
        boolean first = true;


        //creamos el query para enviar el post
        result.append("&");

        result.append(URLEncoder.encode("id_receta", "UTF-8"));
        result.append("=");
        result.append(URLEncoder.encode(idReceta, "UTF-8"));

        result.append("&");

        result.append(URLEncoder.encode("id_paso", "UTF-8"));
        result.append("=");
        result.append(URLEncoder.encode(r.getId()+"", "UTF-8"));

        result.append("&");

        result.append(URLEncoder.encode("descripcion", "UTF-8"));
        result.append("=");
        result.append(URLEncoder.encode(r.getDescripcion(), "UTF-8"));

        result.append("&");

        result.append(URLEncoder.encode("urlimage", "UTF-8"));
        result.append("=");
        result.append(URLEncoder.encode(r.getUrlImagen(), "UTF-8"));


        //devolvemos los datos formateados para poder ser enviados en el post
        return result.toString();
    }

    //este metodo crea los diferentes parametrso que tendra el ingredeinte de la receta que se pase como id

    private static String getQueryRecetaIngrediente(Ingrediente r,String idReceta) throws UnsupportedEncodingException {
        StringBuilder result = new StringBuilder();
        boolean first = true;


        //creamos el query para enviar el post
        result.append("&");

        result.append(URLEncoder.encode("id_receta", "UTF-8"));
        result.append("=");
        result.append(URLEncoder.encode(idReceta, "UTF-8"));

        result.append("&");

        result.append(URLEncoder.encode("id_ingrediente", "UTF-8"));
        result.append("=");
        result.append(URLEncoder.encode(r.getId()+"", "UTF-8"));

        result.append("&");

        result.append(URLEncoder.encode("cantidad", "UTF-8"));
        result.append("=");
        result.append(URLEncoder.encode(r.getCantidad()+"", "UTF-8"));


        //devolvemos los datos formateados para poder ser enviados en el post
        return result.toString();
    }


    //crea los parametros para subir las imagenes
    private static String getQuerySubeImagen(Bitmap image) throws UnsupportedEncodingException {


        //trasfromamos la imagen bitmap a bases64

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.PNG, 30, stream);
        byte[] byteFormat = stream.toByteArray();
        // get the base 64 string
        String imgString = Base64.encodeToString(byteFormat, Base64.DEFAULT);


        //creamos el query para enviar el post
        //System.out.println(imgString);

        StringBuilder result = new StringBuilder();

        result.append("&");
        result.append(URLEncoder.encode("imagen", "UTF-8"));
        result.append("=");
        result.append(URLEncoder.encode("data:image/png;base64,"+imgString,"UTF-8"));



        return result.toString();

    }

    //crea el put en el servidro
    public static String sendPost(String urlWeb, String tipo, String query){


        //primero creamos una coenxion
        HttpURLConnection connection;
        //DataOutputStream request = null;
        OutputStreamWriter request = null;


        URL url = null;
        String response = null;
        //String parameters = "username="+""+"&password="+"";

        try {
            //damos permiso a la aplicacion para que accdea a internet
            if (android.os.Build.VERSION.SDK_INT > 9) {
                StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                StrictMode.setThreadPolicy(policy);
            }
            //creamos la url
            url = new URL(urlWeb);
            //creamos la conexion y le damos los parametros para realizar el post
            connection = (HttpURLConnection) url.openConnection();
            connection.setDoOutput(true);

            //si la peticion tiene llamadas form-data no se usara el paramteo Content-Type
            if (!tipo.equalsIgnoreCase("multipart/form-data")){
                connection.setRequestProperty("Content-Type", tipo);
            }
            //configuramos la conexion
            connection.setRequestMethod("POST");
            connection.setReadTimeout(90000);
            connection.setConnectTimeout(95000);


            //realizamos el post
            OutputStream os = connection.getOutputStream();
            BufferedWriter writer = new BufferedWriter( new OutputStreamWriter(os, "UTF-8"));
            writer.write(query);
            writer.flush();
            writer.close();


            connection.connect();

            //mostramos el resultado del post

            int responseCode=connection.getResponseCode();

            if (responseCode == HttpsURLConnection.HTTP_OK) {
                String idIngredeinte;
                response = "";
                BufferedReader br=new BufferedReader(new InputStreamReader(connection.getInputStream()));
                while ((idIngredeinte=br.readLine()) != null) {
                    response+=idIngredeinte;

                }
            }
            else {
                response="";

            }
            os.close();
            System.out.println("el id es:"+response);

            return response;

        }catch(MalformedURLException error) {
            System.out.println("error: "+error.getLocalizedMessage());

        }
        catch(SocketTimeoutException error) {

            System.out.println("error: "+error.getLocalizedMessage());
        }
        catch (IOException error) {
            System.out.println("error: "+error.getLocalizedMessage());


        }

        return response;
    }

}
