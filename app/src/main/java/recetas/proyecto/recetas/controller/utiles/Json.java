package recetas.proyecto.recetas.controller.utiles;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by javi on 03/03/2018.
 */

public class Json {
    public static String leeJsonDesdeLaURL(String urlString){
        StringBuilder sb = new StringBuilder();
        try {
            URL url = new URL(urlString);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

            BufferedReader br = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));

            String line;
            while ((line = br.readLine()) != null) {
                sb.append(line+"\n");
            }
            br.close();

        } catch (IOException e) {
            System.out.print(e.getMessage());
        }
        return sb.toString();
    }
}
