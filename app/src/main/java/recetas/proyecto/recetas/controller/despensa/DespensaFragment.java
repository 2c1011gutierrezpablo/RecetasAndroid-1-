package recetas.proyecto.recetas.controller.despensa;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetSequence;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;

import recetas.proyecto.recetas.MainActivity;
import recetas.proyecto.recetas.R;
import recetas.proyecto.recetas.controller.utiles.Json;
import recetas.proyecto.recetas.model.constantes.SharedConstantes;
import recetas.proyecto.recetas.model.despensa.Ingrediente;

public class DespensaFragment extends Fragment {
    //constante y valor de los argumentos que se usaran al construir este fragemnt
    private static final String ARG_ITEM = "item";
    private String item;

    //elemtos de la vista
    private RecyclerView recyclerView;
    private DespensaAdapter adapter;
    private ConstraintLayout layoutNoHayIng;
    private ProgressBar cargando;
    //lista de ingredientes
    private ArrayList<Ingrediente> listaIngredientes;

    public DespensaFragment() {

    }

    //constructor estatico que devuelve una nueva instancia de esta clase
    public static DespensaFragment newInstance(String item) {
        DespensaFragment fragment = new DespensaFragment();
        Bundle args = new Bundle();
        args.putString(ARG_ITEM, item);
        fragment.setArguments(args);
        return fragment;
    }

    //al crearse la isntancia asignamos los argumentos que hemos definido en el onstructor estatico e inicializamos la lista de ingredientes
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        item = getArguments().getString(ARG_ITEM);
        listaIngredientes = ((MainActivity)getActivity()).despensaData.getListaIngredientes();
    }

    //conseguimos los elemttos de la vista y la cofigutamos
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //inflamos la vista con el XML correspondiente
        View vista = inflater.inflate(R.layout.fragment_despensa, container, false);
        //conseguimos los elemttos
        recyclerView = (RecyclerView) vista.findViewById(R.id.recycler_view);
        layoutNoHayIng = (ConstraintLayout) vista.findViewById(R.id.constraintNoHayIngListaCompra);
        cargando = (ProgressBar) vista.findViewById(R.id.cargando_despensa_asinc);

        //añadimos una acción al floating action button
        FloatingActionButton fab = (FloatingActionButton) vista.findViewById(R.id.fab_despensa);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity) getActivity()).showFragment(BuscarIngredienteFragment.newInstance(getString(R.string.add_ingredients_pantry),0));
            }
        });

        //conseguimos la imagen de fondo que se mostrará solo si la lista está vacia
        ImageButton fondo = (ImageButton) vista.findViewById(R.id.add_food_to_your_pantry);
        fondo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).showFragment(BuscarIngredienteFragment.newInstance(getString(R.string.add_ingredients_pantry),0));
            }
        });
        //Creamos el adapter y el recyclerview
        adapter = new DespensaAdapter(listaIngredientes,true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);



        //conseguimos el shred preferences
        SharedPreferences prefs = vista.getContext().getSharedPreferences(SharedConstantes.nombre, Context.MODE_PRIVATE);
        //si el campo usuario existe significa que está logeado, entonces cargamos su despensa Online mediante una peticion a nuestro servidor
        String usuarioGuardado = prefs.getString(SharedConstantes.usuario, null);
        if (usuarioGuardado!=null) {
            new PeticionAsincrona().execute("http://reasype.com/api/despesaDelUsuario.php?usuario="+usuarioGuardado);
        } else {
            //ponemos fondo al recycler si la lista está vacia
            ponFondoAlRecycler();
        }

        //si el atributo tutorialDespensa del shared preferences es false, mostramos el tutorialDespensa (para que solo se muestre la primera vez)
        Boolean tutorialDone = prefs.getBoolean(SharedConstantes.tutorialDespensa, false);
        if (!tutorialDone) {
            new TapTargetSequence(getActivity())
                    .targets(  // Specify the target radius (in dp)
                            TapTarget.forView(vista.findViewById(R.id.fab_despensa), getString(R.string.welcome), getString(R.string.add_ingredients_tutorial))
                                    // All options below are optional
                                    .outerCircleColor(R.color.colorAccent)      // Specify a color for the outer circle
                                    .outerCircleAlpha(0.96f)            // Specify the alpha amount for the outer circle
                                    .targetCircleColor(R.color.blanco)   // Specify a color for the target circle
                                    .titleTextSize(24)                  // Specify the size (in sp) of the title text
                                    .titleTextColor(R.color.blanco)      // Specify the color of the title text
                                    .descriptionTextSize(14)            // Specify the size (in sp) of the description text
                                    .descriptionTextColor(R.color.blanco)  // Specify the color of the description text
                                    .textColor(R.color.blanco)            // Specify a color for both the title and description text
                                    .drawShadow(true)                   // Whether to draw a drop shadow or not
                                    .cancelable(false)                  // Whether tapping outside the outer circle dismisses the view
                                    .tintTarget(false)                   // Whether to tint the target view's color
                                    .transparentTarget(true)           // Specify whether the target is transparent (displays the content underneath)// Specify a custom drawable to draw as the target
                                    .targetRadius(60)
                    ).start();
            SharedPreferences.Editor editor = vista.getContext().getSharedPreferences(SharedConstantes.nombre, Context.MODE_PRIVATE).edit();
            editor.putBoolean(SharedConstantes.tutorialDespensa, true); //ponemos el valor a false para que no se vulva a mostar
            editor.apply();
        }
        return vista;
    }
    //comprobamos si la lista está vacia o no para poner el fondo en funcion de eso
    private void ponFondoAlRecycler(){
        if (((MainActivity)getActivity()).despensaData.getListaIngredientes().size()==0){
            layoutNoHayIng.setVisibility(View.VISIBLE);
        } else {
            layoutNoHayIng.setVisibility(View.INVISIBLE);
        }
    }
    //metodo para guardar la instancia una vez de reemplanza el fragment
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }
    //metodo que se ejecuta cuando se crea el fragment despues de configurar la vista
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setUpActionBar();
    }
    //conseguimos y configuramos la actionbar
    @SuppressWarnings("ConstantConditions")
    private void setUpActionBar() {
        ActionBar actionBar = ((MainActivity) getActivity()).getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(false);
        actionBar.setTitle(item);
        //setHasOptionsMenu(true);
    }
    //inflamos el fragemnt con el menu
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_add, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }
    //definimos las acciones de los menús
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                getActivity().onBackPressed();
                return true;
            case R.id.anadir:
                ((MainActivity) getActivity()).showFragment(BuscarIngredienteFragment.newInstance(getString(R.string.add_ingredients_pantry),0));
                return true;
            case R.id.delete:
                if (listaIngredientes.size()>0){
                    ((MainActivity) getActivity()).despensaData.deleteIngredientesSeleccionados();
                    adapter.notifyDataSetChanged();
                    ponFondoAlRecycler();
                } else {
                    Toast.makeText(getContext(),getString(R.string.no_ingredients_to_remove),Toast.LENGTH_SHORT).show();
                }

                return true;
        }
        return super.onOptionsItemSelected(item);
    }
    //petición asincrona que devuleve los ingredientes de un usuario logeado en concreto y los añade a la vista
    protected class PeticionAsincrona extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... strings) {
            return Json.leeJsonDesdeLaURL(strings[0]);
        }
        @Override
        protected void onPostExecute(String json) {
            Ingrediente[] ingredientes = new Gson().fromJson(json, Ingrediente[].class);
            if (ingredientes!=null){
                listaIngredientes.clear();
                listaIngredientes.addAll((Arrays.asList(ingredientes)));
                adapter.notifyDataSetChanged();
            }
            cargando.setVisibility(View.GONE);
            layoutNoHayIng.setVisibility(View.INVISIBLE);
            //ponemos fondo al recycler si la lista está vacia
            ponFondoAlRecycler();
        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            cargando.setMax(100);
            cargando.setVisibility(View.VISIBLE);
        }
    }


}