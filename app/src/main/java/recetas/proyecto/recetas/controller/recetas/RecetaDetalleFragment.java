package recetas.proyecto.recetas.controller.recetas;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.io.Serializable;

import recetas.proyecto.recetas.MainActivity;
import recetas.proyecto.recetas.R;
import recetas.proyecto.recetas.controller.utiles.Utiles;
import recetas.proyecto.recetas.model.constantes.Servidor;
import recetas.proyecto.recetas.model.recetas.Receta;

public class RecetaDetalleFragment extends Fragment {
    //constante y valor de los argumentos que se usaran al construir este fragemnt
    private static final String ARG_ITEM = "item";
    private static final String ARG_RECETA = "receta";
    private String item;
    private Receta receta;

    //elementos de la vista
    private ImageView imagenReceta;
    private TextView textoDescripcion;
    private TextView textoIngredientes;
    private TextView textoNumPasos;

    public RecetaDetalleFragment() {
    }
    //constructor estatico al cual le pasamos los argumentos deseados (item y receta)
    public static RecetaDetalleFragment newInstance(String item, Receta receta) {
        RecetaDetalleFragment fragment = new RecetaDetalleFragment();
        Bundle args = new Bundle();
        args.putString(ARG_ITEM, item);
        args.putSerializable(ARG_RECETA, (Serializable) receta);
        fragment.setArguments(args);
        return fragment;
    }
    //cuando se cree una nueva instancia, recuperamos los argumentos que hemos pasado al constructor estatico y los asignamos (receta y titulo)
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        item = getArguments().getString(ARG_ITEM);
        receta = (Receta) getArguments().getSerializable(ARG_RECETA);
    }

    //Creamos y configuramos la vista
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //infamos la vista con el XML correpsondiente
        View vista = inflater.inflate(R.layout.fragment_receta_detalle, container, false);
        //conseguimos los elemnetos de la cista
        textoDescripcion = (TextView)vista.findViewById(R.id.descripcion_receta_detalle);
        textoIngredientes = (TextView)vista.findViewById(R.id.ingredientes_receta_detalle);
        textoNumPasos = (TextView)vista.findViewById(R.id.numPasosRecetaDetalle);
        ImageView imagen = (ImageView) vista.findViewById(R.id.imagen_receta_detalle);

        //asignamos la información correspondiente a estos elementos
        textoDescripcion.setText(receta.getDescripcion());
        textoIngredientes.setText(Utiles.arrayListIngredientesToString(receta.getIngredientes()));
        textoNumPasos.setText(receta.getPasos().size()+"");
        //descargamos la imagen con piccaso (libreria)
        Picasso.with(getContext()).load(Servidor.host+receta.getUrlImagen()).into(imagen);
        //recuperamos el boton "comanzar receta" y le asignamos la acción correspondiente para abrir el detallde de los pasos
        Button comenzarReceta = (Button) vista.findViewById(R.id.comenzarReceta);
        comenzarReceta.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ((MainActivity) getContext()).showFragment(PasoRecetasFragment.newInstance(getString(R.string.step)+" 1",receta));
            }
        });
        return vista;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setUpActionBar();

    }
    //configuramos la actionbar
    @SuppressWarnings("ConstantConditions")
    private void setUpActionBar() {
        ActionBar actionBar = ((MainActivity) getActivity()).getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp);
        actionBar.setTitle(receta.getNombre());
        //setHasOptionsMenu(true);
    }
    //inflamos el menu
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // inflater.inflate(R.menu.menu_add, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }
    //definimos las acciones correspondientes al menu
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                getActivity().onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }



}