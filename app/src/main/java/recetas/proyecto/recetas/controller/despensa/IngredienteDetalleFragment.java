package recetas.proyecto.recetas.controller.despensa;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.io.Serializable;

import recetas.proyecto.recetas.MainActivity;
import recetas.proyecto.recetas.R;
import recetas.proyecto.recetas.model.constantes.Servidor;
import recetas.proyecto.recetas.model.despensa.Ingrediente;

public class IngredienteDetalleFragment extends Fragment {
    //constante y valor de los argumentos que se usaran al construir este fragemnt
    private static final String ARG_ITEM = "item";
    private String item;
    private static final String ARG_INGREDIENTE = "ingrediente";
    private Ingrediente ingrediente;

    public IngredienteDetalleFragment() {
    }

    //constructor estatico al cual le pasamos el item (titulo) y el ingrediente (ingrediente serializable)
    public static IngredienteDetalleFragment newInstance(String item,Ingrediente ingrediente) {
        IngredienteDetalleFragment fragment = new IngredienteDetalleFragment();
        Bundle args = new Bundle();
        args.putString(ARG_ITEM, item);
        args.putSerializable(ARG_INGREDIENTE, (Serializable) ingrediente);
        fragment.setArguments(args);
        return fragment;
    }
    //al crearse recuperamos los argumentos que hemos definido en el constructor estatico y los asignamos
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        item = getArguments().getString(ARG_ITEM);
        ingrediente = (Ingrediente) getArguments().getSerializable(ARG_INGREDIENTE);
    }

    //creamos y configuramos la vista
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //inflamos la vista con el XML correspondiente
        View vista = inflater.inflate(R.layout.fragment_ingrediente_detalle, container, false);

        //conseguimos los elementos de la vista y asignamos los valores correspondientes
        ImageView imagen = (ImageView) vista.findViewById(R.id.imagen_detalle_ing);
        TextView descripcion = (TextView) vista.findViewById(R.id.descripcion_detalle_ing);
        TextView qty  = (TextView) vista.findViewById(R.id.qty_detalle_ing);

        //descargamos la imagen con piccaso y la mostramos en "imagen"
        Picasso.with(getContext()).load(Servidor.host+ingrediente.getUrlImagen()).into(imagen);
        descripcion.setText(ingrediente.getNombre());
        qty.setText(String.format("%s %s", ingrediente.getCantidad(), ingrediente.getUnidad()));
        return vista;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setUpActionBar();
    }
    //configuramos la actionbar
    @SuppressWarnings("ConstantConditions")
    private void setUpActionBar() {
        ActionBar actionBar = ((MainActivity) getActivity()).getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp);
        actionBar.setTitle(ingrediente.getNombre());
        //setHasOptionsMenu(true);
    }
    //infalmos el menu
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }
    //definimos las acciones del menu
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                getActivity().onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


}