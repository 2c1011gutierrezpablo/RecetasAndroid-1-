package recetas.proyecto.recetas.controller.recetas;

import recetas.proyecto.recetas.MainActivity;
import recetas.proyecto.recetas.R;
import recetas.proyecto.recetas.controller.utiles.Utiles;
import recetas.proyecto.recetas.model.constantes.Servidor;
import recetas.proyecto.recetas.model.recetas.Receta;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by javi on 25/01/2018.
 */


public class RecetaAdapter extends RecyclerView.Adapter<RecetaAdapter.Viewholder> {

    private ArrayList<Receta> listaRecetas; //la lista de recetas
    private Receta receta;
    private Context context;

    public RecetaAdapter(ArrayList<Receta> listaRecetas, Context context) {
        this.listaRecetas = listaRecetas;
        this.context = context;
    }

    public class Viewholder extends RecyclerView.ViewHolder  implements View.OnClickListener  {

        //declaramos los elementos de nuestra vista
        public ImageView imagen;
        public TextView titulo;
        public TextView ingredientes;

        public View v;


        public Viewholder(View itemView) {
            super(itemView);
            //definiomos un listener de click
            itemView.setOnClickListener(this);

            //recuperaos los elementos de nuestra vista (XML del adapter)
            imagen = (ImageView)itemView.findViewById(R.id.urlImagen_receta);
            titulo = (TextView)itemView.findViewById(R.id.tituloIngrediente);
            ingredientes = (TextView)itemView.findViewById(R.id.ingredientes_receta_Adapter);

            this.v = itemView;
        }

        @Override
        public void onClick(View v) {
            //cuando se clicke un elemento se lanzará el fragment de detalle con la receta correspondiente a la posicón de la pulsación
            ((MainActivity) v.getContext()).showFragment(RecetaDetalleFragment.newInstance(v.getContext().getString(R.string.detail), listaRecetas.get(getLayoutPosition())));
        }

    }

    @Override
    public Viewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        //conseguimos, inflamos y devolvemos la vista correspondiente al XML del adapter de recetas
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_recetas, parent, false);

        return new Viewholder(itemView);
    }

    @Override
    public void onBindViewHolder(Viewholder holder, int position) {
        //asignamos la información correspondiente a cada elemento del adapter
        receta = listaRecetas.get(position);

        holder.titulo.setText(receta.getNombre());
        holder.ingredientes.setText(Utiles.arrayListIngredientesToString(receta.getIngredientes()));

        Picasso.with(holder.v.getContext()).load(Servidor.host+ receta.getUrlImagen()).into(holder.imagen);
    }

    @Override
    public int getItemCount() {
        return listaRecetas.size();
    }


}
