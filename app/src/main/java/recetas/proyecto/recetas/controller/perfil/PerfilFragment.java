package recetas.proyecto.recetas.controller.perfil;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Arrays;

import recetas.proyecto.recetas.MainActivity;
import recetas.proyecto.recetas.R;
import recetas.proyecto.recetas.controller.utiles.Json;
import recetas.proyecto.recetas.model.constantes.SharedConstantes;
import recetas.proyecto.recetas.model.despensa.Ingrediente;

public class PerfilFragment extends Fragment {
    //constante y valor de los argumentos que se usaran al construir este fragemnt
    private static final String ARG_ITEM = "item";
    private String item;

    //elemtos de la vista
    private  ImageButton perfil;
    private ConstraintLayout layout;
    private EditText usuario;
    private EditText contra;
    private ProgressBar cargando;
    private String user,pass;
    private TextView textoAMostrar;
    private Button cerrarsesion;
    private View v;

    public PerfilFragment() {
    }

    //constructor esattico al cual le pasamos el item (titulo del fragment)
    public static PerfilFragment newInstance(String item) {
        PerfilFragment fragment = new PerfilFragment();
        Bundle args = new Bundle();
        args.putString(ARG_ITEM, item);
        fragment.setArguments(args);
        return fragment;
    }

    //cuando se cree una instancia se ejecutará este étodo en el cual recuperamos el los elementos que hemos definido en el constructor estatico y los asignamos
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        item = getArguments().getString(ARG_ITEM);
    }

    //Creamos y configuramos la vista
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //inflamos la vista con el XML correspondiente
        View vista = inflater.inflate(R.layout.fragment_perfil, container, false);

        //Recuperamos los elementos de la vista
        perfil = (ImageButton) vista.findViewById(R.id.imagenperfiluser);
        textoAMostrar = (TextView) vista.findViewById(R.id.nombre_usuario);
        layout = (ConstraintLayout) vista.findViewById(R.id.inicioSesion);
        usuario = (EditText) vista.findViewById(R.id.ed_user);
        contra = (EditText) vista.findViewById(R.id.ed_pass);
        v= vista;
        cargando = (ProgressBar) vista.findViewById(R.id.cargando_perfil);

        //definisms las acciones de los botones
        cerrarsesion = (Button) vista.findViewById(R.id.cerrar_sesion);
        cerrarsesion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { //cuando se clique cerrar sesión, eliminamos el usuario de sharedpreferences y ocultamos el layout
                SharedPreferences preferences = v.getContext().getSharedPreferences(SharedConstantes.nombre, 0);
                preferences.edit().remove(SharedConstantes.usuario).apply();

                layout.setVisibility(View.VISIBLE);
                cerrarsesion.setVisibility(View.GONE);
                textoAMostrar.setText("Log in");
            }
        });

        Button iniciarsesion = (Button) vista.findViewById(R.id.button_login);
        iniciarsesion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { //cuando se pulse iniciar sesión, conseguimos los datos de los EditText y lanzamos una petición a nuestro servidor para comprobar el usuario
                user=usuario.getText().toString();
                pass=contra.getText().toString();
                new PeticionAsincrona().execute("http://reasype.com/api/autenticaUsuario.php?usuario="+user+"&pass="+pass);
            }
        });

        //comprobamos si existe el usuario para saber si mostrar el inicio de sisón o el layout de cerrar sisión
        SharedPreferences prefs = vista.getContext().getSharedPreferences(SharedConstantes.nombre, Context.MODE_PRIVATE);
        String usuarioGuardado = prefs.getString(SharedConstantes.usuario, null);
        if (usuarioGuardado==null) {
            cerrarsesion.setVisibility(View.GONE);
        } else {
            layout.setVisibility(View.GONE);
            textoAMostrar.setText("Bienvenido "+usuarioGuardado);
        }
        return vista;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setUpActionBar();

    }

    //configuramos la actionbar
    @SuppressWarnings("ConstantConditions")
    private void setUpActionBar() {
        ActionBar actionBar = ((MainActivity) getActivity()).getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(false);
        actionBar.setTitle(item);
    }

     @Override
     public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
     }

     //definimos las acciones cuando se pulsen los elementos del menu
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                getActivity().onBackPressed();
                return true;
            case R.id.anadir:
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    //petición asincrona al srevidor para comprobar si existe el usuario
    protected class PeticionAsincrona extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String... strings) {
            return Json.leeJsonDesdeLaURL(strings[0]);
        }
        //conseguimos los datos devuletos y si existe en la base de datos lo añadimos al shredpreferences y mostramos la información correspondiente
        @Override
        protected void onPostExecute(String json) {
            cargando.setVisibility(View.GONE);
            JSONArray jsonResultadoInicio = null;
            try {
                jsonResultadoInicio = new JSONArray(json);
                String existe = jsonResultadoInicio.getJSONObject(0).getString("existe");
                if (existe.equals("1")){
                    SharedPreferences.Editor editor = v.getContext().getSharedPreferences(SharedConstantes.nombre, Context.MODE_PRIVATE).edit();
                    editor.putString(SharedConstantes.usuario,user);
                    editor.apply();

                    textoAMostrar.setText(user);
                    cerrarsesion.setVisibility(View.VISIBLE);
                    layout.setVisibility(View.GONE);
                    Toast.makeText(v.getContext(),getString(R.string.login_succesfully),Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(v.getContext(),getString(R.string.user_pass_error),Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                Toast.makeText(v.getContext(),getString(R.string.error_login),Toast.LENGTH_SHORT).show();
            }

        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            cargando.setMax(100);
            cargando.setVisibility(View.VISIBLE);
        }
    }
}