package recetas.proyecto.recetas.model.constantes;

/**
 * Created by javi on 03/03/2018.
 */

public class SharedConstantes {
    public static final String nombre = "reasype";
    public static final String usuario = "usuario";
    public static final String tutorialDespensa = "tutorialDoneDespensa";
    public static final String tutorialRecetas = "tutorialDoneRecetas";
    public static final String tutorialLista = "tutorialDoneLista";
}
