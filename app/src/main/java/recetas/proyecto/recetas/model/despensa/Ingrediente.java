package recetas.proyecto.recetas.model.despensa;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

/**
 * Created by javi on 16/1/18.
 */

public class Ingrediente implements Serializable, Parcelable {

    public int id;
    public String nombre;
    public String urlImagen;
    public float cantidad;
    public String unidad;

    public boolean estoySeleccionado = false;

    public Ingrediente(int id, String nombre, float cantidad, String unidad) {
        this.id = id;
        this.nombre = nombre;
        this.cantidad = cantidad;
        this.unidad = unidad;
    }

    public Ingrediente(int id, String nombre, float cantidad, String unidad, String urlImagen, boolean estoySeleccionado) {
        this.id = id;
        this.nombre = nombre;
        this.cantidad = cantidad;
        this.unidad = unidad;
        this.urlImagen = urlImagen;
        this.estoySeleccionado = estoySeleccionado;
    }

    protected Ingrediente(Parcel in) {
        id = in.readInt();
        nombre = in.readString();
        cantidad = in.readFloat();
        unidad = in.readString();
        estoySeleccionado = in.readByte() != 0;
    }

    public static final Creator<Ingrediente> CREATOR = new Creator<Ingrediente>() {
        @Override
        public Ingrediente createFromParcel(Parcel in) {
            return new Ingrediente(in);
        }

        @Override
        public Ingrediente[] newArray(int size) {
            return new Ingrediente[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public float getCantidad() {
        return cantidad;
    }

    public void setCantidad(float cantidad) {
        this.cantidad = cantidad;
    }

    public String getUnidad() {
        return unidad;
    }

    public void setUnidad(String unidad) {
        this.unidad = unidad;
    }

    public String getUrlImagen() {
        return urlImagen;
    }

    public void setUrlImagen(String urlImagen) {
        this.urlImagen = urlImagen;
    }

    @Override
    public int describeContents() {
        return this.hashCode();
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeString(nombre);
        parcel.writeFloat(cantidad);
        parcel.writeString(unidad);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj.getClass()!=Ingrediente.class){
            return false;
        }
        if (((Ingrediente)obj).getId()==this.getId()){
            return true;
        }
       return false;
    }

    public boolean isEstoySeleccionado() {
        return estoySeleccionado;
    }

    public void setEstoySeleccionado(boolean estoySeleccionado) {
        this.estoySeleccionado = estoySeleccionado;
    }

    @Override
    public String toString() {
        return getNombre();
    }
}
