package recetas.proyecto.recetas.model.compra;

import java.util.ArrayList;

import recetas.proyecto.recetas.model.despensa.Ingrediente;

/**
 * Created by javi on 15/02/2018.
 */

public class CompraDataController {

    private ArrayList<Ingrediente> listaIngredientes;

    public CompraDataController() {
        this.listaIngredientes = new ArrayList<>();
    }

    public boolean addIngrediente(Ingrediente ing){
        if ( listaIngredientes.contains(ing)){
            return false;
        }
        listaIngredientes.add(ing);
        return true;
    }

    public ArrayList<Ingrediente> getListaIngredientes() {
        return listaIngredientes;
    }

    public void deleteIngredientesSeleccionados(){
        for (int i = listaIngredientes.size()-1; i>=0;i--){
            if (listaIngredientes.get(i).isEstoySeleccionado()){
                listaIngredientes.remove(i);
            }
        }
    }
}
