package recetas.proyecto.recetas.model.recetas;

/**
 * Created by javi on 18/02/2018.
 */

public class Paso {

    private int id;
    private String descripcion;
    private String urlImagen;

    public Paso(int id, String descripcion, String urlImagen) {
        this.id = id;
        this.descripcion = descripcion;
        this.urlImagen = urlImagen;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getUrlImagen() {
        return urlImagen;
    }

    public void setUrlImagen(String urlImagen) {
        this.urlImagen = urlImagen;
    }
}
