package recetas.proyecto.recetas.model.despensa;

import java.util.ArrayList;

import recetas.proyecto.recetas.model.despensa.Ingrediente;

/**
 * Created by javi on 10/02/2018.
 */

public class DespensaDataController {

    private ArrayList<Ingrediente> listaIngredientes;


    public DespensaDataController() {
        listaIngredientes = new ArrayList<>();
    }

    public DespensaDataController(ArrayList<Ingrediente> listaIngredientes) {
        this.listaIngredientes = listaIngredientes;
    }

    public boolean addIngrediente(Ingrediente ing){
        if ( listaIngredientes.contains(ing)){
            return false;
        }
        listaIngredientes.add(ing);
        return true;
    }

    public ArrayList<Ingrediente> getListaIngredientes() {
        return listaIngredientes;
    }

    public boolean addIngredienteSelecionado(Ingrediente ing){
        if ( listaIngredientes.contains(ing)){
            return false;
        }
        listaIngredientes.add(ing);
        return true;
    }

    //eliminamos de la lista todos los ingredientes que estén seleccionados
    public void deleteIngredientesSeleccionados(){
        for (int i = listaIngredientes.size()-1; i>=0;i--){
            if (listaIngredientes.get(i).isEstoySeleccionado()){
                listaIngredientes.remove(i);
            }
        }
    }

}
