package recetas.proyecto.recetas.model.recetas;

import java.util.ArrayList;

import recetas.proyecto.recetas.model.despensa.Ingrediente;

/**
 * Created by javi on 10/02/2018.
 */

public class RecetasDataController {
    public ArrayList<Receta> listaRecetas;

    public ArrayList<Ingrediente> listaIngredientesAñadir;//sera una lista con los ingredientes a guardar en una receta

    public RecetasDataController() {
        listaRecetas = new ArrayList<>();
        listaIngredientesAñadir = new ArrayList<>();


    }

    public RecetasDataController(ArrayList<Receta> listaRecetas) {
        this.listaRecetas = listaRecetas;
    }

    public boolean addReceta(Receta receta){
        listaRecetas.add(receta);
        return true;
    }

    public ArrayList<Receta> getListaRecetas() {
        return listaRecetas;
    }



    public ArrayList<Ingrediente> getListaIngredientesAñadir() {
        return listaIngredientesAñadir;
    }

    public void borrarlistaIngredientesAñadir(){

        listaIngredientesAñadir = listaIngredientesAñadir = new ArrayList<>();

    }
}
