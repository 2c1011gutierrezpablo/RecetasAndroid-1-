package recetas.proyecto.recetas.model.recetas;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.ArrayList;

import recetas.proyecto.recetas.model.despensa.Ingrediente;

public class Receta implements Serializable, Parcelable {

    private int id;
    private String urlImagen, nombre, descripcion;
    private ArrayList<Paso> pasos;
    private ArrayList<Ingrediente> ingredientes;

    public Receta(int id, String urlImagen, String nombre, String descripcion) {
        this.id = id;
        this.urlImagen = urlImagen;
        this.nombre = nombre;
        this.descripcion = descripcion;
    }

    public Receta(int id, String urlImagen, String nombre, String descripcion, ArrayList<Paso> pasos, ArrayList<Ingrediente> ingredientes) {
        this.id = id;
        this.urlImagen = urlImagen;
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.pasos = pasos;
        this.ingredientes = ingredientes;
    }

    protected Receta(Parcel in) {
        id = in.readInt();
        urlImagen = in.readString();
        nombre = in.readString();
        descripcion = in.readString();
        ingredientes = in.createTypedArrayList(Ingrediente.CREATOR);
    }

    public static final Creator<Receta> CREATOR = new Creator<Receta>() {
        @Override
        public Receta createFromParcel(Parcel in) {
            return new Receta(in);
        }

        @Override
        public Receta[] newArray(int size) {
            return new Receta[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUrlImagen() {
        return urlImagen;
    }

    public void setUrlImagen(String urlImagen) {
        this.urlImagen = urlImagen;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public ArrayList<Paso> getPasos() {
        return pasos;
    }

    public void setPasos(ArrayList<Paso> pasos) {
        this.pasos = pasos;
    }

    public ArrayList<Ingrediente> getIngredientes() {
        return ingredientes;
    }

    public void setIngredientes(ArrayList<Ingrediente> ingredientes) {
        this.ingredientes = ingredientes;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(urlImagen);
        dest.writeString(nombre);
        dest.writeString(descripcion);
        dest.writeTypedList(ingredientes);
    }
}